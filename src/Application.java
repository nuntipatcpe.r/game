import armor.Armor;
import armor.HeavyArmor;
import armor.LightArmor;
import armor.MediumArmor;
import backpack.*;
import character.*;
import character.Character;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main (String[] args){

//        DSix aDice = new DSix();
//        for(int i = 0; i<10 ; i++){
//            System.out.println( aDice.roll() );
//        }

        Armor chainMail = new HeavyArmor("Chain Mail", 8);
        Armor plateMail = new HeavyArmor("Plate Mail", 10);
        Armor leatherArmor = new MediumArmor("Leather Armor", 7);
        Armor robe = new LightArmor("Robe", 3);

        Item potionItem = new HpPotionItem();
        Item mjolnir= new Mjolnir();
        Item manaPotion= new ManaPotion();
        List<Item> itemList = new ArrayList<>();
//
        for(int i =0 ; i<10 ; i++) {
            itemList.add(potionItem);
        }
//        for(int i =0 ; i<0 ; i++) {
//            itemList.add(manaPotion);
//        }
//        for(int i =0 ; i<0 ; i++) {
//            itemList.add(mjolnir);
//        }

        Backpack b = new Backpack(itemList);

        b.addItem(potionItem);
        b.addItem(manaPotion);
        b.addItem(manaPotion);
        b.addItem(mjolnir);
        b.addItem(mjolnir);
        b.addItem(manaPotion);
        b.addItem(manaPotion);


//
//        ArrayList<Integer> allWeightItems = new ArrayList<>();
//        ArrayList<String> allNameItems = new ArrayList<>();
//
//        allWeightItems.add(potionItem.weightItem());
//        allWeightItems.add(mjolnir.weightItem());
//        allWeightItems.add(manaPotion.weightItem());
//
//        allNameItems.add(potionItem.nameItem());
//        allNameItems.add(mjolnir.nameItem());
//        allNameItems.add(manaPotion.nameItem());
        //     Backpack a = new Backpack(allWeightItems,allNameItems);

        try {
            CharacterClass class1 = new Rogue();
            Character character_1 = new Character("นักรบโรมัน", class1);
            System.out.println("Class  " + class1.getName());
            // System.out.println(a.toString());
            System.out.println(b.toString());


//            boolean canWearArmor = class1.wearArmor(chainMail);
//            boolean canWearArmor_1 = class1.wearArmor(plateMail);
//            boolean canWearArmor_2 = class1.wearArmor(leatherArmor);
//            boolean canWearArmor_3 = class1.wearArmor(robe);
//            System.out.println("Can Wear ChainMail: " + canWearArmor);
//            System.out.println("Can Wear PlateMail: " + canWearArmor_1);
//            System.out.println("Can Wear LeatherArmor: " + canWearArmor_2);
//            System.out.println("Can Wear Robe: " + canWearArmor_3);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
