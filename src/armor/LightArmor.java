package armor;

public class LightArmor implements Armor{

    private String name;
    private int ac;

    public LightArmor(String name, int ac){
        this.name = name;
        this.ac = ac;
    }

    @Override
    public int getSpeedPenalty(){
        return 0;
    }

    @Override
    public int getAC() {
        return this.ac;
    }


}
