package armor;

import character.CharacterClass;
import character.Rogue;

public   class HeavyArmor implements Armor{

    private String name;
    private int ac;

    public HeavyArmor(String name, int ac){
        this.name = name;
        this.ac = ac;

    }

    @Override
    public int getSpeedPenalty(){
        return -2;
    }

    @Override
    public int getAC() {
        return this.ac;
    }

}
