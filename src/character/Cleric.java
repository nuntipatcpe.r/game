package character;

import armor.Armor;

public class Cleric implements CharacterClass{
    @Override
    public int getHpDie() {
        return 8;
    }

    @Override
    public String getName() {
        return "Cleric";
    }

    @Override
    public boolean wearArmor(Armor armor) {
        // ใส่เกราะได้ทุกชนิด
        return true;
    }
}
