package character;

import armor.Armor;
import armor.HeavyArmor;
import armor.LightArmor;
import armor.MediumArmor;

public class Rogue implements CharacterClass{

    public Rogue(){


    }
    @Override
    public int getHpDie() {
        return 6;
    }

    @Override
    public String getName() {
        return "Rogue";
    }

    @Override
    public boolean wearArmor(Armor armor) {
//        Armor medium = new MediumArmor("2",5);
//        Armor light = new LightArmor("2",5);
//       if(medium.getSpeedPenalty()==armor.getSpeedPenalty()||light.getSpeedPenalty()==armor.getSpeedPenalty())
//       {
//           return true;
        // };
        if(armor instanceof HeavyArmor )
        {
            return false;
        }
//        System.out.println(armor instanceof  MediumArmor);
//        System.out.println(armor instanceof LightArmor );
//        System.out.println(armor instanceof HeavyArmor );

        // ใส่เกราะได้เฉพาะ　Medium, Light
        return true;
    }
}
