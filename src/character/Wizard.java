package character;

import armor.Armor;
import armor.LightArmor;
import armor.MediumArmor;

public class Wizard implements CharacterClass{

    @Override
    public int getHpDie() {
        return 4;
    }

    @Override
    public String getName() {
        return "Wizard";
    }

    @Override
    public boolean wearArmor(Armor armor) {
        // ใส่เกราะได้เฉพาะ　Light
        Armor light = new LightArmor("2",5);
//        if(light.getSpeedPenalty()==armor.getSpeedPenalty())
//        {
//            return true;
//        };
        if( armor instanceof LightArmor )
        {
            return true;
        }
        // ใส่เกราะได้เฉพาะ　Medium, Light
        return false;
    }
}
