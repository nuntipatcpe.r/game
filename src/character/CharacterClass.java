package character;

import armor.Armor;

public interface CharacterClass {
    int getHpDie();
    String  getName();
    boolean wearArmor(Armor armor);
}
