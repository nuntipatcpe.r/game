package character;

import dice.DSix;
import dice.Die;

import java.util.Scanner;

import java.io.IOException;

public class Character {
    private enum status{
        strength,dexterity,constitution,charisma,wisdom,intelligence;
    }
    private String name;
    private CharacterClass characterClass;
    private int level=1;
    private int maxHp;
    private int currentHp=0;
    private int strength=0;
    private int dexterity=0;
    private int constitution=0;
    private int charisma=0;
    private int wisdom=0;
    private int intelligence=0;
    private boolean n=true;
    private int nRoll,nFace,f,bonus,punishment;
    Integer[] sumArray = new Integer[100];
    status dex =  status.dexterity;
    public Character(String name,CharacterClass characterClass) throws IOException {
       this.characterClass = characterClass;
        this.name = name;
        System.out.println(name);
        System.out.println("อาชีพ "+characterClass.getName());
        System.out.println("ทอยเต๋ากำหนดค่าสถานะ ...");
        System.out.println("กดปุ่ม>>Enter<<เพื่อเริ่มทอย ...");
        System.in.read();
        randomStats(6,6);
       addAllStatusFirst();
//      addAuto();
        System.out.println("Enter to look status");
        System.in.read();
        allStats();

        constitution = 0;
        System.out.println("constitution  "+constitution);
        if(constitution>=10){
            bonus(constitution);
            this.maxHp = rollDie(characterClass.getHpDie())+this.bonus;
            System.out.println("MaxHp Random  "+(maxHp-bonus));
            System.out.println("ADD +"+bonus);
            System.out.println("MaxHp "+maxHp);
        }
        else {
            punishment(constitution);
            this.maxHp = rollDie(characterClass.getHpDie())-this.punishment;
            System.out.println("maxHp Random  "+(maxHp+punishment));
            System.out.println("Remove -"+punishment);
           //System.out.println("MaxHp "+maxHp);
            hpNoZero(maxHp);
            System.out.println("MaxHp "+maxHp);
        }
    }
    private int hpNoZero(int currentHp){
        if(currentHp<=0){
            this.maxHp=1;
        }
        return this.maxHp;
    }
    private int bonus(int count) {
        for(int i = count-10; i >0;i--){
            if(i%2==0){
                this.bonus++;
            }
        }
        // this.bonus=(count-10)/2;
        return this.bonus;
    }
    private int punishment(int count){

        for (int i = 0; i > count-10; i--) {
            if (i%2 == 0) {
                this.punishment++;
            }
        }

        //this.punishment=((count*(-1))/2)+5;
        return punishment;
    }
    private int rollDie(int x){
        Die dSix = new DSix(x);
        return  dSix.roll();
    }
    private void randomStats(int nRoll, int nFace){
        this.nRoll=nRoll;
        this.nFace=nFace;
        Integer[] scoreArray = new Integer[4];
        Integer sum ;

        for(int i=0; i<this.nRoll; i++){

            sum = 0;
            for(int j=0; j<4; j++){
                scoreArray[j] =  rollDie(nFace);
                sum += scoreArray[j];
            }

            Integer lowestScore = this.nRoll-1;
            for(int j=0; j<4; j++){
                if(scoreArray[j] < lowestScore){
                    lowestScore = scoreArray[j];
                }
            }

            sum -= lowestScore;
            sumArray[i] = sum;
            f=i+1;
            System.out.println("ค่าที่ทอยครั่งที่  " +  f + "  ได้  ");
            for(int k=1;k<=4;k++) {
                System.out.print(scoreArray[k - 1] + " ");
            }

            System.out.println("  ผลรวมไม่นับค่าต่ำสุด ... " + sumArray[i]);

        }

    }
    private void printAllStatus(){
        System.out.println(status.wisdom+" "+wisdom);
        System.out.println(status.charisma+" "+charisma);
        System.out.println(status.constitution+" "+constitution);
        System.out.println(status.strength+" "+strength);
        System.out.println(status.intelligence+" "+intelligence);
        System.out.println(dex+" "+dexterity);
        System.out.println("___________________________");

    }
    private void addAllStatusFirst(){
        String[] stat ={" DEX "," INT "," STR "," CTT "," CRM "," WD "};
        int dex=0,inT=1,str=2,ctt=3,crm=4,wd=5;
        Scanner Sc = new Scanner(System.in);
        for(int a =0; a<6;a++) {
            System.out.println("Roll__"+(a+1)+"  Is equal to "+ sumArray[a]);
            System.out.println(" ADD "+stat[0]+stat[1]+stat[2]+stat[3]+stat[4]+stat[5]);
            n=true;
            while (n){
                String s = Sc.nextLine();

                switch (s) {
                    case "DEX":
                        if(dexterity==0) {
                            dexterity += sumArray[a];

                            System.out.println("ADD  "+status.dexterity+" " + sumArray[a]);
                            stat[dex]="";
                        }else if (dexterity!=0) {
                            a--;
                            System.out.println("This status has already been used");
                        }
                        n=false;

                        break;
                    case "INT":
                        if(intelligence==0) {
                            intelligence += sumArray[a];
                            System.out.println("ADD  "+ status.intelligence+" "+ sumArray[a]);
                            stat[inT]="";
                        }else if (intelligence!=0) {
                            a--;
                            System.out.println("This status has already been used");
                        }
                        n = false;
                        break;
                    case "STR":
                        if(strength==0) {
                            strength += sumArray[a];
                            System.out.println("ADD  "+status.strength+" " + sumArray[a]);
                            stat[str]="";
                        }else if ( strength!=0) {
                            a--;
                            System.out.println("This status has already been used");
                        }
                        n = false;
                        break;
                    case "CTT":
                        if(constitution==0) {
                            constitution += sumArray[a];
                            System.out.println("ADD "+status.constitution +" "+ sumArray[a]);
                            stat[ctt]="";
                        }else if ( constitution!=0) {
                            a--;
                            System.out.println("This status has already been used");

                        }n = false;
                        break;
                    case "CRM":
                        if(charisma==0) {
                            charisma += sumArray[a];
                            System.out.println("ADD  "+status.charisma+" "+ sumArray[a]);
                            stat[crm]="";

                        }else if ( charisma!=0) {
                            a--;
                            System.out.println("This status has already been used");

                        }n = false;
                        break;
                    case "WD":
                        if(wisdom==0) {
                            wisdom += sumArray[a];
                            System.out.println("ADD  " +status.wisdom+" "+ sumArray[a]);
                            stat[wd]="";
                        }else if ( wisdom!=0) {
                            a--;
                            System.out.println("This status has already been used");


                        } n = false;
                        break;
                    default:
                        System.out.println(" Keyword to add "+stat[0]+stat[1]+stat[2]+stat[3]+stat[4]+stat[5]);
                        a--;
                        n = false;
                        break;
                }
            }
            if(a<6){
                printAllStatus();
            }
        }

    }
    private void addAuto() {
        dexterity +=sumArray[0];
        strength += sumArray[1];
        intelligence += sumArray[2];
        wisdom += sumArray[3];
        constitution += sumArray[4];
        charisma += sumArray[5];
    }
    private void allStats(){
        System.out.println("Hi"+name);
        System.out.println("Class  "+characterClass.getName());
        System.out.println("Your status ");
        System.out.println(""+status.wisdom+" "+wisdom);
        System.out.println(""+status.charisma+" "+charisma);
        System.out.println(""+status.constitution+" "+constitution);
        System.out.println(""+status.strength+" "+strength);
        System.out.println(""+status.intelligence+" "+intelligence);
        System.out.println(""+status.dexterity+" "+dexterity);
        System.out.println("___________________________");

    }
}