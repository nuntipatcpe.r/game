package character;

import armor.Armor;

public class Fighter implements CharacterClass{
    @Override
    public int getHpDie() {
        return 10;
    }

    @Override
    public String getName() {
        return "Fighter";
    }

    @Override
    public boolean wearArmor(Armor armor) {
        // ใส่เกราะได้ทุกชนิด

        return true;
    }
}
