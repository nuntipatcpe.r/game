package backpack;

public class HpPotionItem implements Item{

    @Override
    public String nameItem() {
        return "potionItem";
    }

    @Override
    public int weightItem() {
        return 2;
    }
}
