package backpack;

public class ManaPotion implements Item{


    @Override
    public String nameItem() {
        return "ManaPotion";
    }

    @Override
    public int weightItem() {
        return 1;
    }
}
