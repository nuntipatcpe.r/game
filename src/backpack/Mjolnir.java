package backpack;

public class Mjolnir implements Item{
    @Override
    public String nameItem() {
        return "Mjolnir";
    }

    @Override
    public int weightItem() {
        return 1000000;
    }
}
