package dice;

import java.util.Random;

public class DSix implements Die{
    private int faceDie;
    public DSix (int faceDie){
        this.faceDie=faceDie;
    }


    @Override
    public int roll() {
        Random random = new Random();
        return random.nextInt(faceDie) + 1;
    }
}
