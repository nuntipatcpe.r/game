package dice;

public interface Die {
    int roll();
}
